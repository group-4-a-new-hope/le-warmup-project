#CS205
#Group 4
#Warmup Project

import sqlite3
import shlex
import csv

#flag variables
data_loaded = False
running = True

#lists of keywords
acceptable_first_keywords = ['planets', 'species', 'return']
acceptable_second_keywords_species = ['classification', 'homeworld', 'climate', 'terrain', 'species_name', 'planet_name']
acceptable_second_keywords_planets = ['climate', 'terrain', 'planet_name']
acceptable_third_keywords_species = ['classification', 'homeworld', 'climate', 'terrain', 'species_name', 'planet_name']
acceptable_third_keywords_planets = ['climate', 'terrain', 'planet_name']

# Splits command string into it's individual parts
def split_input(command):
    try:
        command_words = shlex.split(command)
        return command_words
    except:
        print("Incorrect format, make sure to complete quotations around items with more than 1 word")

# Input validation
def validate_input(command_words):
    try:
        # Validates help command
        if command_words[0] == 'help':
            return True

        # Validates meta command
        if command_words[0] == 'table':
            return True

        # Validates exit command
        if command_words[0] == 'exit':
            return True

        # Validates first keyword
        if command_words[0] not in acceptable_first_keywords:
            print("Invalid command, queries must start with keywords: 'planets', 'species' or 'return'")
            return False

        # Validates species acceptable keywords
        if command_words[0] == 'species' and command_words[1] not in acceptable_second_keywords_species:
            print("Invalid command, acceptable species keywords: ")
            print(acceptable_second_keywords_species)
            return False

         # Validates species_name
        if command_words[0] == 'species' and command_words[2] not in acceptable_third_keywords_species:
            print("Invalid command, make sure to use species_name")
            return False

        # Validates planet acceptable keywords
        if command_words[0] == 'planets' and command_words[1] not in acceptable_second_keywords_planets:
            print("Invalid command, acceptable planet keywords: ")
            print(acceptable_second_keywords_planets)
            return False

        # Validates planets_name
        if command_words[0] == 'planets' and command_words[2] not in acceptable_third_keywords_planets:
            print("Invalid command, make sure to use planet_name")
            return False

        # Completes validation if query is complete
        if len(command_words) == 4 and command_words[0] != 'return':
            return True

        # Validation for planet or species query with too many parts
        if len(command_words) != 4 and (command_words[0] == 'planet' or command_words[0] == 'species'):
            print("Query is not of the right size, did you forget to use quotes around items with two words?")
            return False

        # Validates returns second keywords
        if command_words[0] == 'return' and command_words[1] not in acceptable_second_keywords_return:
            print("Invalid command, acceptable return keywords: ")
            print(acceptable_second_keywords_return)
            return False

        # Validates boolean sign usage
        if command_words[0] == 'return' and command_words[3] not in acceptable_logical_symbol:
            print("Invalid command, acceptable logic symbols: ")
            print(acceptable_logical_symbol)
            return False

        # Validates equals comparison keyword for species
        if command_words[1] == 'species' and command_words[3] == '=' and command_words[
            2] not in acceptable_third_keywords_return_species_equals:
            print("Invalid command, acceptable return species '=' keywords: ")
            print(acceptable_third_keywords_return_species_equals)
            return False

        # Validates equals comparison keyword for planet
        if command_words[1] == 'planet' and command_words[3] == '=' and command_words[
            2] not in acceptable_third_keywords_return_planet_equals:
            print("Invalid command, acceptable return species '=' keywords: ")
            print(acceptable_third_keywords_return_planet_equals)
            return False

    except:
        print("Missing keywords, fields cannot be left blank, make sure query is complete.")
        return False

#Validates commands pre-data being loaded in
def validate_load(command_words):
    # Validates load data command
    if command_words[0] == 'load' and command_words[1] == 'data':
        return True

    # Validates help command
    if command_words[0] == 'help':
        return True

    # Validates meta command
    if command_words[0] == 'table':
        return True

    # Validates exit command
    if command_words[0] == 'exit':
        return True

    else:
        return False

#initial creation of tables
def create_tables():
    # creates the species table
    try:
        conn = sqlite3.connect('SW.db')
        c = conn.cursor()
        c.execute('''CREATE TABLE species
                        (
                        species_name text,
                        classification text,
                        homeworld text,
                        PRIMARY KEY (species_name)
                        )''')
        conn.commit()
        print("Species table created.");
    except BaseException:
        print("Table already exists.")
    finally:
        if c is not None:
            c.close()
        if conn is not None:
            conn.close()

    # creates the planet table
    try:
        conn = sqlite3.connect('SW.db')
        c = conn.cursor()
        c.execute('''CREATE TABLE planets
                        (
                        planet_name text,
                        climate text,
                        terrain text,
                        PRIMARY KEY (planet_name)
                        )''')
        conn.commit()
        print("Planet table created.");
    except BaseException:
        print("Table already exists")
    finally:
        if c is not None:
            c.close()
        if conn is not None:
            conn.close()

#inserts records from csv into tables
def load_tables():
    #loads species table
    file = open("species.csv", mode='r')
    conn = sqlite3.connect('SW.db')
    c = conn.cursor()
    try:
        species_data = file.readlines()
        for i in species_data:
            data_to_insert = i.strip('\n')
            data_to_insert = data_to_insert.split(',', 2)
            c.execute("INSERT INTO species VALUES (?, ?, ?)", data_to_insert)
            conn.commit()
        print("Data successfully added to species")
    except sqlite3.DatabaseError:
        print("Record already exists")
    if c is not None:
        c.close()
    if conn is not None:
        conn.close()
    file.close()

#loads planet table
    file = open("planets.csv", mode='r')
    conn = sqlite3.connect('SW.db')
    c = conn.cursor()
    try:
        planets_data = file.readlines()
        for i in planets_data:
            data_to_insert = i.strip('\n')
            data_to_insert = data_to_insert.split(',', 2)
            c.execute("INSERT INTO planets VALUES (?, ?, ?)", data_to_insert)
            conn.commit()
        print("Data succesfully added to planets")
    except sqlite3.DatabaseError:
        print("Record already exists")
    if c is not None:
        c.close()
    if conn is not None:
        conn.close()
    file.close()

#initializes the database
def initialize():
    print("initializing")
    #checks if tbales already created
    global data_loaded
    if data_loaded == False:
        create_tables()
    load_tables()
    data_loaded = True

#processes the search
def query(input):
    #checks if database already created, loads if not
    if data_loaded == False:
        initialize()

    #used for JOIN, checks given table, joins the other
    joint = ''
    if input[0] == "planets":
        joint = "species"
    elif input[0] == "species":
        joint = "planets"

    #the query string assembled from input
    query_term = "SELECT " + input[1] + " FROM " + input[0] + " LEFT JOIN " + joint + " ON species.homeworld=planets.planet_name WHERE " + input[2] + " LIKE '" + input[3] + "';"

    # processes the database query and displays results
    conn = sqlite3.connect('SW.db')
    c = conn.cursor()
    try:
        c.execute(query_term)
        count = 0
        list = []
        for row in c.fetchall():
            list.append(row)
            count += 1
        if count == 0:
            print("No results. Perhaps the archives are incomplete.")
        #return_list = list.split(', ')
        print(list)
    except sqlite3.DatabaseError:
        print("Error. Could not retrieve data.")
    if c is not None:
        c.close()
    if conn is not None:
        conn.close()

#processes metadata command
def table(input):
    term = input[1]
    if term == "planets":
        term = term[:-1]
    query_term = ("SELECT COUNT(" + term + "_name) FROM " + input[1])

    conn = sqlite3.connect('SW.db')
    c = conn.cursor()
    try:
        c.execute(query_term)
        for row in c.fetchall():
            print("Table Size: " + str(row[0]))
    except sqlite3.DatabaseError:
        print("Error. Could not retrieve data.")
    if c is not None:
        c.close()
    if conn is not None:
        conn.close()

#displays instructions
def help():
    print("Enter 'load data' to initialize the database")
    print("Enter 'table' and a table name for table size metadata")
    print("There are three primary keywords: planets, species, and return")
    print("---------------------------------------------------------------------------------------------------------------------")
    print("Planet Keyword")
    print("An example of a planet call is: 'planets terrain planet_name Hoth' ")
    print("This will return the planet named Hoth's terrain")
    print("Options for second keyword after planets are: ")
    print("climate and terrain")
    print("---------------------------------------------------------------------------------------------------------------------")
    print("Species Keyword")
    print("An example of a species call is: 'species homeworld species_name Hutt' ")
    print("Any of the following keywords will work after species:")
    print("classification and homeworld")
    print("---------------------------------------------------------------------------------------------------------------------")

#interprets the user input
def parse(user_input):
    # figure out what command is
    # then call get_data() function based on input
    if user_input[0] == "species":
        found = False
        for item in acceptable_second_keywords_species:
            if user_input[1] == item:
                found = True
                query(user_input)
        if found == False:
            print("invalid input")
    elif user_input[0] == "planets":
        found = False
        for item in acceptable_second_keywords_planets:
            if user_input[1] == item:
                found = True
                query(user_input)
        if found == False:
            print("invalid input")
    #processes non-query commands
    elif user_input[0] == "help":
        help()
    elif user_input[0] == "table":
        table(user_input)
    elif user_input[0] == "load":
        initialize()
    elif user_input[0] == "exit":
        global running
        running = False

def main():
    print("System Active")
    print("-------------")
    while running:
        valid = False
        valid_load = False
        while valid == False:
            # Validate first set of commands
            while not data_loaded:
                while not valid_load:
                    user_input = input("Enter command: ")
                    user_input = user_input.lower()
                    user_input = split_input(user_input)
                    valid_load = validate_load(user_input)
                    print('Please first load data')

                parse(user_input)
                valid_load = False

            # get user input
            user_input = input("Enter command: ")
            # make lowercase
            user_input = user_input.lower()
            # create list
            user_input = split_input(user_input)
            # validate the input
            valid = validate_input(user_input)

        parse(user_input)

    print("System closed.")


main()

